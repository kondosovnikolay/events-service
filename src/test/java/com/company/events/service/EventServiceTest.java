package com.company.events.service;

import com.company.events.entity.Event;
import com.company.events.repository.EventRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.util.UUID;

import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EventServiceTest {

    private EventService sut;
    private EventRepository eventRepository;

    @BeforeEach
    void init() {
        eventRepository = mock(EventRepository.class);
        sut = new EventService(eventRepository);
    }

    @Test
    void returnsEvent() {
        //given
        UUID eventId = UUID.randomUUID();
        Event event = new Event(eventId, "name", "data", OffsetDateTime.now(), OffsetDateTime.now());
        //when
        when(eventRepository.findById(eq(eventId))).thenReturn(of(event));
        Event result = sut.getById(eventId);
        //then
        assertThat(result).isEqualTo(event);
    }

    /*
    @Test
    void mustFail() {
        //given
        UUID eventId = UUID.randomUUID();
        Event event = new Event(eventId, "name", "data", OffsetDateTime.now(), OffsetDateTime.now());
        //when
        when(eventRepository.findById(eq(eventId))).thenReturn(of(event));
        Event result = sut.getById(eventId);
        //then
        assertThat(result).isEqualTo(null);
    }
     */

}
