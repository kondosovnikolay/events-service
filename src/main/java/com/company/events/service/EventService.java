package com.company.events.service;

import com.company.events.entity.Event;
import com.company.events.repository.EventRepository;
import com.company.events.request.CreateEventRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class EventService {

    private final EventRepository eventRepository;

    public Event createEvent(CreateEventRequest createEventRequest) {
        return eventRepository.save(new Event(
                UUID.randomUUID(),
                createEventRequest.getEventName(),
                createEventRequest.getData(),
                OffsetDateTime.now(),
                OffsetDateTime.now())
        );
    }

    public Event getById(UUID id) {
        return eventRepository.findById(id).orElse(null);
    }

}
