package com.company.events.controller;

import com.company.events.entity.Event;
import com.company.events.request.CreateEventRequest;
import com.company.events.service.EventService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@AllArgsConstructor
public class EventController {

    private final EventService eventService;

    @PostMapping("/api/v1/event")
    public Event createNew(@RequestBody @Valid CreateEventRequest createEventRequest) {
        return eventService.createEvent(createEventRequest);
    }

    @GetMapping("/api/v1/event/{id}")
    public Event getById(@PathVariable UUID id) {
        return eventService.getById(id);
    }
}
