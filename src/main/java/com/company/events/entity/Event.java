package com.company.events.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.OffsetDateTime;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Event {

    @Id
    private UUID id;
    @Column(name = "event_name")
    private String eventName;
    private String data;
    @Column(name = "created_ts")
    private OffsetDateTime createdTs;
    @Column(name = "updated_ts")
    private OffsetDateTime updatedTs;
}
