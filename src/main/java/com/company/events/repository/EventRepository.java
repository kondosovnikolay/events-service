package com.company.events.repository;

import com.company.events.entity.Event;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface EventRepository extends PagingAndSortingRepository<Event, UUID> {
}
