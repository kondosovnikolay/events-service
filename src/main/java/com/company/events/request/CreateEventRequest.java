package com.company.events.request;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@RequiredArgsConstructor
public class CreateEventRequest {

    @NotEmpty
    private final String eventName;
    private final String data;

}
